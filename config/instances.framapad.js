f$_config = 'local';

f$_jquery = 'fQuery';

if (f$_not_in_frame) {
    f$_extra_css = true;
} else {
    f$_bootstrap_css = false;
}

// Force la désactivation des fenêtres modales, du bandeau et du macaron
f$_alert_text = '';
f$_alert_modal_text = '';
f$_modal_don_liendl = '';
f$_donate = false;

f$_host = 'ovh';
f$_credits = 'framapad';
