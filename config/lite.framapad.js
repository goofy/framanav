f$_config = 'local';

f$_jquery = 'fQuery';

f$_extra_css = true;

// Force la désactivation des fenêtres modales, du bandeau et du macaron
f$_alert_text = '';
f$_modal_don_liendl = '';
f$_donate = false;

// Message arrêt de la création des pads
f$_alert_modal_title = 'Création des pads désactivée';
f$_alert_modal_text = '<p>Nous vous informons que cette instance de Framapad (<b>lite</b>.framapad.org) ne peut plus accueillir de nouveaux pads.<p>'+
 '<br /><p>Il reste bien évidement possible de travailler sur les pads déjà existants (ils ne seront pas supprimés)'+
 ' mais pour en créer de nouveaux, veuillez passer par la page d’accueil du site :<br />'+
 '<a href="http://www.framapad.org">www.framapad.org</a></p>'+
 '<br /><p>Merci.<br />L’équipe technique</p>';

f$_host = 'ovh';
f$_credits = 'framapad';
